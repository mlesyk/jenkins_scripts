#!/bin/sh
#
echo -n "kernel_pkgname = "; echo ${kernel_pkgname="linux-image-3.16.0-45-generic"}
echo -n "kernel_patches = "; echo ${kernel_patches=""}
echo -n "kernel_changes = "; echo ${kernel_changes="Ordinary Ubuntu kernel"}
echo -n "kernel_postfix = "; echo ${kernel_postfix="megacorp"}

kernel_maintainer="Jenkins <jenkins.lesyk.org>"
kernel_version=`echo $kernel_pkgname | cut -d '-' -f 3`
kernel_abi=`echo $kernel_pkgname | cut -d '-' -f 4`
kernel_flavour=`echo $kernel_pkgname | cut -d '-' -f 5`

sudo apt-get update
cat jenkins_scripts/buildTrustyKernel/stuff/packages.list | xargs sudo apt-get install -y
sudo apt-get -o Dir::Etc::SourceList=./jenkins_scripts/buildTrustyKernel/stuff/sources.list update
apt-get -o Dir::Etc::SourceList=./jenkins_scripts/buildTrustyKernel/stuff/sources.list source linux-image-$kernel_version-$kernel_abi-$kernel_flavour

mkdir build
cd ./linux*
mv * ../build/
cd ../build

if [ "$kernel_patches" != "" ]; then
  patch_counter=0
  kernel_changes="$kernel_changes; Patches applied:"
  for patch in $kernel_patches
    do
      wget -O "./patch$patch_counter.patch" $patch
      patch -p1 <patch$patch_counter.patch
      patch_counter=$((patch_counter+1))
      kernel_changes="$kernel_changes $patch"
    done
fi

chmod -R u+x debian/scripts

mv debian.master debian_master

if [ -d debian.* ]; then
  original_name=`head -n 1 debian.*/changelog | cut -d ' ' -f 1`
  kernel_subversion=`head -n 1 debian.*/changelog | cut -d '(' -f 2 | cut -d '.' -f 4 | cut -d ')' -f 1 | cut -d '~' -f 1`
  kernel_postfix_subversion=$((kernel_subversion+1))
  DEBEMAIL="$kernel_maintainer" dch --package $original_name -c debian.*/changelog -v $kernel_version-$kernel_abi.$kernel_postfix_subversion~$kernel_postfix$BUILD_NUMBER $kernel_changes
  sed -i '0,/UNRELEASED/{s/UNRELEASED/trusty/}' debian.*/changelog
else
  original_name=`head -n 1 debian_master/changelog | cut -d ' ' -f 1`
  kernel_subversion=`head -n 1 debian_master/changelog | cut -d '(' -f 2 | cut -d '.' -f 4 | cut -d ')' -f 1 | cut -d '~' -f 1`
  kernel_postfix_subversion=$((kernel_subversion+1))
  DEBEMAIL="$kernel_maintainer" dch --package $original_name -c debian_master/changelog -v $kernel_version-$kernel_abi.$kernel_postfix_subversion~$kernel_postfix$BUILD_NUMBER $kernel_changes
  sed -i '0,/UNRELEASED/{s/UNRELEASED/trusty/}' debian_master/changelog
  cp -ax debian_master/abi/* debian_master/abi/$kernel_version-$kernel_abi.$kernel_subversion
fi
mv debian_master debian.master

fakeroot debian/rules updateconfigs
fakeroot debian/rules clean
